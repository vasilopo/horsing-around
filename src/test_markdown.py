#! /usr/bin/env python

from create_publication_list import output_formatted_entry
from create_publication_list import MDbold
from create_publication_list import MDheader
from create_publication_list import MDlink
from create_publication_list import MDstrikethrough


def test_MDbold():
    """ MDbold should return a Markdown Bold formatted version of the string.
    """

    output = MDbold('Something in bold')
    expected = '**Something in bold**'

    assert output == expected


def test_MDheader():
    """ MDheader should return a Markdown Header formatted version of the string.
    Test for a level 21 header
    """
    output = MDheader('Top-level header', 1)
    expected = '# Top-level header'

    assert output == expected


def test_MDlink():
    """ MDlink should return a Markdown formatted link.
    """

    output = MDlink('link text', 'http://qwantz.com/index.php?comic=3371')
    expected = '[link text](http://qwantz.com/index.php?comic=3371)'

    assert output == expected


def test_MDstrikethrough():
    """ MDstrikethrough should return a Markdown Strikethrough formatted
    version of the string.
    """

    output = MDstrikethrough('something no longer true')
    expected = '~~something no longer true~~'

    assert output == expected


def test_title_format():
    """ Titles should be formatted as Bold Markdown.
    """
    formatted_entry = output_formatted_entry(
        'Meier H.',
        'Historic treasures of Swiss horse breeding',
        'Schweiz Arch Tierheilkd',
        '2017',
        '10.17236',
        '28059058'
    )
    expected = "**Historic treasures of Swiss horse breeding**"

    year, entry = formatted_entry

    assert expected in entry


def test_pubmed_link_format():
    """ Links should be generated as Markdown links to PubMed.
    """
    formatted_entry = output_formatted_entry(
        'Meier H.',
        'Historic treasures of Swiss horse breeding',
        'Schweiz Arch Tierheilkd',
        '2017',
        '10.17236',
        '28059058'
    )
    expected = "[28059058](https://www.ncbi.nlm.nih.gov/pubmed/?term=28059058)"

    year, entry = formatted_entry

    assert expected in entry


def test_journal_format():
    """ Journals should be formatted as Markdown Italics.
    """
    formatted_entry = output_formatted_entry(
        'Meier H.',
        'Historic treasures of Swiss horse breeding',
        'Schweiz Arch Tierheilkd',
        '2017',
        '10.17236',
        '28059058'
    )

    expected = '*Schweiz Arch Tierheilkd*'

    year, entry = formatted_entry

    assert expected in entry
